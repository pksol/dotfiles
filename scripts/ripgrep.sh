#!/bin/bash
if ! rg_loc="$(type -p rg)" || [[ -z $rg_loc ]]; then
  curl -LO https://github.com/BurntSushi/ripgrep/releases/download/0.8.1/ripgrep_0.8.1_amd64.deb
  sudo dpkg -i ripgrep_0.8.1_amd64.deb
  rm -f ripgrep_0.8.1_amd64.deb
fi


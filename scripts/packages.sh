#!/bin/bash

common_packages=( automake cmake cmus ctags curl deluge emacs feh ghc git htop ipython lynx make neovim python3 python3-pip python-setuptools ranger tmux zsh )

### Arch ###
if uname -r | grep -q ARCH; then
  # Install common packages
  for package in "${common_packages[@]}"
  do
    sudo pacman -S --noconfirm --needed $package
  done

  # Install non-common packages
  sudo pacman -S --noconfirm --needed \
    nodejs npm \
    openssh \
    ripgrep \
    stack

  # Install trizen if not already installed
  if ! trizen_loc="$(type -p trizen)" || [[ -z $trizen_loc ]]; then
    sudo pacman -Syu
    git clone https://aur.archlinux.org/trizen.git repos/trizen
    cd repos/trizen
    makepkg -si
  fi

### macOS ###
elif uname -s | grep -q Darwin; then
  # Install Homebrew if not already installed
  if ! brew_loc="$(type -p brew)" || [[ -z $brew_loc ]]; then
    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
  fi

  brew update
  brew install fzf rg

### Debian-based systems ###
else
  # Install common packages
  for package in "${common_packages[@]}"
  do
    sudo apt install -y $package
  done

  # Install non-common packages
  sudo apt install -y \
    fonts-font-awesome \
    haskell-stack \
    software-properties-common
fi

if command -v npm >/dev/null 2>&1; then
  # Install npm global packages
  npm i -g \
    eslint eslint-plugin-vue \
    elm elm-test elm-oracle elm-format \
    tern
fi

pip3 install jedi
pip3 install neovim

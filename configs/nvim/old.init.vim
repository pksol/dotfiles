let g:polyglot_disabled=[]
source $HOME/.config/nvim/config/initialize.vim
source $HOME/.config/nvim/config/general.vim
source $HOME/.config/nvim/config/plugins.vim
source $HOME/.config/nvim/config/keys.vim

--local fn = vim.fn       				-- Call Vim functions
local cmd = vim.cmd     				-- Execute Vim commands
local exec = vim.api.nvim_exec 	-- Execute Vimscript
local g = vim.g         				-- Global variables
local o = vim.o
local opt = vim.opt         		-- Set options (global/buffer/windows-scoped)

-- General
g.mapleader = ','               -- Change leader to a comma
opt.mouse = 'a'                 -- Enable mouse support
opt.clipboard = 'unnamedplus'   -- Copy/paste to system clipboard
opt.swapfile = false            -- Don't use swapfile
opt.wb = false
opt.encoding = 'utf-8'
opt.fileencoding = "utf-8"

-- Color Scheme
opt.termguicolors = true
o.background = "dark"
cmd [[colorscheme gruvbox]]

-- UI
opt.number = true               -- Show line number
opt.showmatch = true            -- Highlight matching parenthesis
opt.foldmethod = 'marker'       -- Enable folding (default 'foldmarker')
opt.colorcolumn = '120'          -- Line lenght marker at 80 columns
opt.splitright = true           -- Vertical split to the right
opt.splitbelow = true           -- Orizontal split to the bottom
opt.ignorecase = true           -- Ignore case letters when search
opt.smartcase = true            -- Ignore lowercase for the whole pattern
opt.linebreak = true            -- Wrap on word boundary
opt.smarttab = true
opt.showcmd = true
opt.autowrite = true
opt.hlsearch = true
opt.so = 7                      -- Set 7 lines to the cursor - when moving vertically using j/k
opt.ruler = true                -- Always show current position

-- Relative numbers when not in insert mode,
-- normal line numbers when in insert mode
exec([[
  augroup numbertoggle
    autocmd!
    autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
    autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
  augroup end
]], false)

-- Memory, CPU
opt.hidden = true               -- Enable background buffers
opt.history = 100               -- Remember N lines in history
opt.lazyredraw = true           -- Faster scrolling
opt.regexpengine = 1
opt.synmaxcol = 240             -- Max column for syntax highlight

-- For regular expressions turn magic on
opt.magic = true

opt.backspace = { "indent", "eol", "start" }
opt.whichwrap = "<,>,h,l"

-- 1 tab == 4 spaces
opt.shiftwidth = 4
opt.tabstop = 4
-- Use spaces instead of tabs
opt.expandtab = true

-- Insert mode completion options
opt.completeopt = 'menuone,noselect'

g.tex_flavor = 'latex'

-- fold
opt.foldmethod = "expr"
opt.foldexpr = "nvim_treesitter#foldexpr()"
opt.foldlevelstart = 99

-- Jump to the last position when reopening a file
cmd [[au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif]]

-- 2 spaces for selected filetypes
cmd [[
  autocmd FileType xml,html,xhtml,css,scss,lua,vue,tex,yaml setlocal shiftwidth=2 tabstop=2 softtabstop=2
]]

-- Python PEP8 indentation standards
cmd [[autocmd FileType py setlocal tabstop=4 softtabstop=4 shiftwidth=4 expandtab autoindent fileformat=unix]]

-- Prevent vim syntax highlighting from getting confused in vue files
cmd [[autocmd FileType vue syntax sync fromstart]]

-- Remove whitespace on save
cmd [[au BufWritePre * :%s/\s\+$//e]]

-- Don't auto comment new lines
cmd [[au BufEnter * set fo-=c fo-=r fo-=o]]

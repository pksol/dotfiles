local lsp = require('lspconfig')
local coq = require('coq')

lsp.sumneko_lua.setup {
    capabilities = vim.lsp.protocol.make_client_capabilities(),
    settings = {
        Lua = {
            runtime = {
		version = 'LuaJIT',
		path = vim.split(package.path, ';')
	    },
	    diagnostics = {
		globals = { 'vim' },
	    },
	    workspace = {
		library = {[vim.fn.expand('$VIMRUNTIME/lua')] = true, [vim.fn.expand('$VIMRUNTIME/lua/vim/lsp')] = true}
	    }
	}
    }
}

lsp.sumneko_lua.setup(coq.lsp_ensure_capabilities{})
vim.cmd('COQnow -s')

call plug#begin('~/.config/nvim/plugged')

" Start screen
Plug 'mhinz/vim-startify'

" Status line
Plug 'itchyny/lightline.vim'
Plug 'maximbaz/lightline-ale'
Plug 'shinchu/lightline-gruvbox.vim'

" Linting
Plug 'w0rp/ale'

" Brackets
Plug 'jiangmiao/auto-pairs'
Plug 'tpope/vim-surround'

" Commenting
Plug 'tomtom/tcomment_vim'

" Buffers
Plug 'qpkorr/vim-bufkill'
Plug 'ap/vim-buftabline'

" File explorer
Plug 'Shougo/unite.vim'
Plug 'Shougo/vimfiler.vim'

" Project management
Plug 'airblade/vim-rooter'
Plug 'tpope/vim-obsession'

" Key binding guide
Plug 'liuchengxu/vim-which-key'

" fzf for fuzzy finding
if has('macunix')
  Plug '/usr/local/opt/fzf'
  Plug '/usr/local/opt/rg'
else
  set rtp+=/usr/bin/fzf
  set rtp+=/usr/bin/rg
endif
Plug 'junegunn/fzf.vim'

" Folding
Plug 'Konfekt/FastFold'

" git plugins
Plug 'tpope/vim-fugitive'
Plug 'junegunn/gv.vim'
Plug 'airblade/vim-gitgutter'

" Fancy searching
Plug 'haya14busa/incsearch.vim'

" ncm2 auto-completion
Plug 'ncm2/ncm2'
Plug 'roxma/nvim-yarp'
Plug 'ncm2/ncm2-bufword'
Plug 'ncm2/ncm2-path'
Plug 'ncm2/ncm2-jedi'

" HTML auto-completion tool
Plug 'mattn/emmet-vim'

" Tmux movement integration
Plug 'christoomey/vim-tmux-navigator'

" Elixir
Plug 'elixir-lang/vim-elixir'
Plug 'thinca/vim-ref'
Plug 'mhinz/vim-mix-format'
Plug 'awetzel/elixir.nvim', { 'do': 'yes \| ./install.sh' }

" Elm
Plug 'elmcast/elm-vim'

" Haskell
Plug 'neovimhaskell/haskell-vim'
Plug 'alx741/vim-hindent'
Plug 'parsonsmatt/intero-neovim'
Plug 'alx741/vim-stylishask'

" LaTeX
Plug 'lervag/vimtex'

" Python
Plug 'vim-scripts/indentpython.vim'
Plug 'davidhalter/jedi-vim'
Plug 'jmcantrell/vim-virtualenv'

" Vue
Plug 'posva/vim-vue'

" General language support
Plug 'sheerun/vim-polyglot'

" Colorschemes
Plug 'flazz/vim-colorschemes'

call plug#end()

filetype plugin indent on
syntax enable

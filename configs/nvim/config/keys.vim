" vim-plug
map <leader>pi :PlugInstall<cr>
map <leader>pu :PlugUpdate<cr>

" Smart way to move between windows
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l

" Ctrl-{hjkl} for navigating out of terminal panes
tnoremap <C-h> <C-\><C-n><C-w>h
tnoremap <C-j> <C-\><C-n><C-w>j
tnoremap <C-k> <C-\><C-n><C-w>k
tnoremap <C-l> <C-\><C-n><C-w>l

" Shift + direction to change tabs
noremap <S-l> gt
noremap <S-h> gT

" buffer mappings
nnoremap <C-N> :bnext<cr>
nnoremap <C-P> :bprev<cr>
map <leader>w :bd<cr>

" for splits
set splitright
set splitbelow
map <leader>vp :vsplit .<cr>
map <leader>hp :split .<cr>

" send deletes to null register
nnoremap <leader>d "_d

" toggle line numbers
map <leader>ln :call ToggleLineNumbers()<cr>

function ToggleLineNumbers()
  if &number
    set nonumber
    set nornu
  else
    set number
    set rnu
  endif
endfunction

" clone paragraph
noremap cp yap<S-}>p

" align paragraph
noremap <leader>a =ip

" paste mode toggle
set pastetoggle=<leader>z

nnoremap c* /\<<C-R>=expand('<cword>')<cr>\>\C<cr>``cgn
nnoremap c# ?\<<C-R>=expand('<cword>')<cr>\>\C<cr>``cgN
nnoremap d* /\<<C-r>=expand('<cword>')<cr>\>\C<cr>``dgn
nnoremap d# ?\<<C-r>=expand('<cword>')<cr>\>\C<cr>``dgN

" VimFiler
map <leader>vf :VimFilerBufferDir<cr>

" fzf
map <leader>o :FZF<cr>
" ripgrep
map <leader>f :Rg<cr>

" Startify
map <leader>s :Startify<cr>

" sudo save to avoid permission denied
command W w !sudo tee % > /dev/null
